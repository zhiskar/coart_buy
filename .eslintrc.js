/*eslint-disable*/

module.exports = {
    root: true,
    env: {
        browser: true,
        node: true,
    },
    extends: [
        '@nuxtjs/eslint-config-typescript',
        'plugin:nuxt/recommended',
    ],
    ignorePatterns: [
        'package.json',
        'tsconfig.json',
    ],
    rules: {
        'nuxt/no-cjs-in-config': 'off',
        'indent': 'off',
        'dot-notation': 'off',
        'no-mixed-spaces-and-tabs': [
            2,
            'smart-tabs',
        ],
        '@typescript-eslint/no-unused-vars': [
            'error',
            {
                'vars': 'all',
                'args': 'after-used',
            },
        ],
        'vue/valid-template-root': 'off',
        'vue/no-v-html': 'off',
        'vue/html-indent': 'off',
        'vue/html-closing-bracket-spacing': [
            'error',
            {
                'endTag': 'never',
                'selfClosingTag': 'never',
            },
        ],
        'vue/html-closing-bracket-newline': [
            'error',
            {
                'singleline': 'never',
                'multiline': 'always',
            },
        ],
        'vue/max-attributes-per-line': [
            'error',
            {
                'multiline': {
                    'max': 1,
                    'allowFirstLine': false,
                },
            },
        ],
        'vue/component-tags-order': [
            'error',
            {
                'order': [['template', 'script'], 'style'],
            },
        ],
        'vue/order-in-components': [
            'error', {
                'order': [
                    'el',
                    'name',
                    'key',
                    'parent',
                    'functional',
                    ['delimiters', 'comments'],
                    ['components', 'directives', 'filters'],
                    'extends',
                    'mixins',
                    ['provide', 'inject'],
                    'ROUTER_GUARDS',
                    'layout',
                    'middleware',
                    'validate',
                    'scrollToTop',
                    'transition',
                    'loading',
                    'inheritAttrs',
                    'model',
                    ['props', 'propsData'],
                    'emits',
                    'setup',
                    'asyncData',
                    'data',
                    'fetch',
                    'head',
                    'computed',
                    'watch',
                    'watchQuery',
                    'LIFECYCLE_HOOKS',
                    'methods',
                    ['template', 'render'],
                    'renderError',
                ],
            },
        ],
        'vue/attributes-order': [
            'error',
            {
                'order': [
                    'DEFINITION',
                    'LIST_RENDERING',
                    'CONDITIONALS',
                    'RENDER_MODIFIERS',
                    'GLOBAL',
                    'UNIQUE',
                    'TWO_WAY_BINDING',
                    'OTHER_DIRECTIVES',
                    'OTHER_ATTR',
                    'EVENTS',
                    'CONTENT',
                ],
                'alphabetical': false,
            },
        ],
        'quotes': [
            'error',
            'single',
        ],
        'semi': [
            'error',
            'always',
        ],
        'comma-dangle': [
            'error',
            'always-multiline',
        ],
        'space-before-function-paren': [
            'error',
            {
                'anonymous': 'always',
                'named': 'never',
                'asyncArrow': 'always',
            },
        ],
        'no-console': 'off',
        'object-curly-spacing': 'off',
    },
}
