module.exports = {
    apps: [
        {
            name: 'coart_buy',
            exec_mode: 'cluster',
            instances: 1,
            script: './node_modules/nuxt/bin/nuxt.js',
            args: 'serve',
        },
    ],
};
