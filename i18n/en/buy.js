module.exports = {
    title: 'Welcome to CoArt Whitelist Sale on Binance Smart Chain!',
    text: '<i>Sale information: the price is with <b>50% discount</b> for the Whitelist sale. You can contribute from 0.1BNB to 5BNB. You will ' +
        'get 50% now, and 50% will be vested for 3 months. We appreciate your contribution!</i>',
    slogan: 'CoArt a bit of Art for everyone!',
    connect: 'Connect',
    amount: {
        label: 'BNB amount',
        placeholder: 'Enter BNB amount',
        error: 'Amount of bnb must be specified from 0.1 to 5',
        noFunds: 'You don\'t have enough funds',
    },
    buy: 'Buy',
    connected: 'You have successful connection via',
    whiteList: 'Am i in whitelist?',
    inWhitelist: 'Your address in the whitelist',
    notInWhitelist: 'Your address is not in the whitelist',
    request: 'Claim remaining tokens',
    successClaim: 'You have successfully claimed funds',
    alreadyClaimed: 'All funds are already claimed',
    error: {
        connect: 'You haven\'t connection to any wallet',
        noAddress: 'No address',
    },
    metamask: 'MetaMask',
    walletconnect: 'WalletConnect',
    needInstallMetamask: 'Install MetaMask',
    claimText: '50% of tokens are vested for 3 months. So, come back here after 3 months to claim you ' +
        'remaining tokens!',
};
