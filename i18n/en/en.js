const footer = require('./footer');
const header = require('./header');
const general = require('./general');
const privacy = require('./privacy');
const terms = require('./terms');
const faq = require('./faq');
const buy = require('./buy');
const index = require('./index');

module.exports = {
    index,
    footer,
    header,
    general,
    privacy,
    terms,
    faq,
    buy,
};
