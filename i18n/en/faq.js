module.exports = {
    title: 'FAQ',
    text: '<h2>What is CoArt Marketplace?</h2>' +
        '<p>The CoArt Marketplace is a marketplace where you can display, collect, buy, sell and trade pieces of ' +
        'unique digital and physical art. We are creating a place where all art lovers and artists can exhibit and ' +
        'sell both an art object and an NFT certificate of that object to fans and collectors. Artists, photographers, ' +
        'writers, musicians and many more use CoArt to trade art online using blockchain technology.</p>' +
        '<h2>What does CoArt Marketplace offer you</h2>' +
        '<p>Proof of ownership and authenticity on the blockchain. CoArt market is a tool to help you digitally sign ' +
        'and upload artwork to the blockchain, creating absolute proof that you are the creator and/or owner of your ' +
        'work.</p>' +
        '<p>Personal showroom. We provide tools to help you easily create a custom showroom that you can then share ' +
        'with your audience to sell your work. The entrance to the CoArt metaverse, where you can meet a huge crowd ' +
        'of connoisseurs and artists, attend exhibitions and concerts, display and trade your own collections around ' +
        'the world.</p>' +
        '<h2>What is the cost</h2>' +
        '<p>CoArt market is free for all members. However, blockchain related transactions are paid by the user. It ' +
        'is also applicable to create a personal multisig wallet based on blockchain technology.</p>' +
        '<p>On successful sales, CoArt market takes a commission of no more than 10% on purchases.</p>' +
        '<h2>Where do I begin</h2>' +
        '<p>You can navigate the CoArt market as much as you like for free. As soon as you decide to participate in ' +
        'a trade, you will be prompted to open an account with us and create a multisig wallet for transferring ' +
        'funds. In a simple step-by-step procedure, you will have the opportunity to create your own personal ' +
        'showroom or buy selected works of art.</p>' +
        '<h2>Do I need to know about blockchain?</h2>' +
        '<p>No special knowledge is required to start trading on the CoArt market. Here you can use your usual ' +
        'credit card payment method, as well as use a wallet on the blockchain. We will easily and simply guide ' +
        'you through the entire process.</p>' +
        '<h2>Contact and support</h2>' +
        '<p>You can reach us at <a href="mailto:coart@coart.biz">coart@coart.biz</a> with any questions or issues.</p>',
};
