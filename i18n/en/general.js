module.exports = {
    toast: {
        error: {
            common: 'Something went wrong. Try again later.',
        },
    },
    tooltip: {
        publicKey: 'This field shows your public key. A public key is a large numerical value used to encrypt data for ' +
            'storing assets of your account, in this case - digital collector tokens.',
        privateKey: 'This field shows your private key. Use your private key to swap tokens, send them as a gift or ' +
            'perform other transactions. The private key is like a password, granting anyone that gains to it control ' +
            'of your account.',
    },
    clipboard: {
        success: 'Copy successful',
        error: 'Copy error',
    },
    notFound: {
        title: 'Error 404',
        button: 'Go to homepage',
    },
};
