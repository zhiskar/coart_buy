module.exports = {
    top: {
        signIn: 'Sign In',
        signUp: 'Sign Up',
        faq: 'FAQ',
        account: 'My account',
        showcases: 'My showcases',
        logout: 'Logout',
    },
    auth: {
        favorites: 'Favorites',
        upload: 'Upload',
        wallet: 'Wallet',
        settings: 'Settings',
        showroom: 'Showroom',
        vr: 'VR Space',
    },
};
