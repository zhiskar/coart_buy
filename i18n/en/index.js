module.exports = {
    top: 'Welcome to CoArt Whitelist Sale on Binance Smart Chain!',
    text: '<i>Sale information: the price is with <b>50% discount</b> for the Whitelist sale. You can contribute from 0.1BNB ' +
        'to 5BNB. You will get 50% now, and 50% will be vested for 3 months. We appreciate your contribution!</i>',
    title: 'CoArt NFT Marketplace',
    slogan: 'a bit art for everyone',
    buy: 'Buy now',
};
