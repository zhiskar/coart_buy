module.exports = {
    title: 'Privacy Policy',
    text: '' +
        '<p>Last Revised: August 3, 2021</p>' +
        '<p>CoArt UAB (“CoArt” “we” or “us”) is committed to protecting your privacy. We have prepared this Privacy ' +
        'Policy to describe to you our practices regarding the Personal Information (as defined below) we collect, why ' +
        'we collect it, and how we use and disclose it.</p>' +
        '<p>Your privacy matters to us, so please do take the time to get to know and familiarize yourself with ' +
        'our policies and practices. Please understand that we reserve the right to change any of our policies and ' +
        'practices at any time, but you can always find the latest version of this Privacy Policy here on this page.</p>' +
        '<p>Topics:</p>' +
        '<ul>' +
        '<li>What data do we collect?</li>' +
        '<li>How do we collect your data?</li>' +
        '<li>How will we use your data?</li>' +
        '<li>How We Share Your Personal Information?</li>' +
        '<li>How do we store your data?</li>' +
        '<li>Marketing</li>' +
        '<li>What are your data protection rights?</li>' +
        '<li>What are cookies?</li>' +
        '<li>How do we use cookies?</li>' +
        '<li>What types of cookies do we use?</li>' +
        '<li>How to manage your cookies</li>' +
        '<li>Privacy policies of other websites</li>' +
        '<li>Changes to our privacy policy</li>' +
        '<li>How to contact us</li>' +
        '<li>How to contact the appropriate authorities</li>' +
        '</ul>' +
        '<h2>What data do we collect?</h2>' +
        '<p>CoArt collects the following data:</p>' +
        '<ul>' +
        '<li><b>Personal identification</b> information (username, email address, Symbol blockchain network account ' +
        'address)</li>' +
        '</ul>' +
        '<h2>How do we collect your data?</h2>' +
        '<p>You directly provide CoArt with most of the data we collect. We collect data and process data when you:</p>' +
        '<ul>' +
        '<li>Register online or place an order for any of our products or services.</li>' +
        '<li>Voluntarily complete a customer survey or provide feedback on any of our message boards or via email.</li>' +
        '<li>Use or view our website via your browser\'s cookies.</li>' +
        '</ul>' +
        '<p>CoArt may also receive the following data indirectly from 3rd parties:</p>' +
        '<ul>' +
        '<li><b>Identification Information</b>, such as name, email, phone number, postal address</li>' +
        '<li><b>Transaction Information</b>, such as public blockchain data (bitcoin, ether, and other Digital Assets ' +
        'are not truly anonymous. We, and any others who can match your public Digital Asset address to other Personal ' +
        'Information about you, may be able to identify you from a blockchain transaction because, in some ' +
        'circumstances, Personal Information published on a blockchain (such as your Digital Asset address and IP ' +
        'address) can be correlated with Personal Information that we and others may have. Furthermore, by using ' +
        'data analysis techniques on a given blockchain, it may be possible to identify other Personal Information ' +
        'about you);</li>' +
        '<li><b>Financial Information</b>, such as bank account information, routing number, credit card number, ' +
        'debit card number; and</li>' +
        '<li><b>Additional Information</b>, at our discretion, to comply with legal obligations.</li>' +
        '</ul>' +
        '<h2>How will we use your data?</h2>' +
        '<p>CoArt collects your data so that we can:</p>' +
        '<ul>' +
        '<li><b>Provide you with our Services</b>. We use your Personal Information to provide you with our ' +
        'Services pursuant to the terms of our Terms of Use.</li>' +
        '<li><b>Comply with legal and regulatory requirements</b>. We process your Personal Information as required ' +
        'by applicable laws and regulations.</li>' +
        '<li><b>Detect and prevent fraud</b>. We process your Personal Information to detect and prevent fraud on ' +
        'your account, which is especially important given the irreversible nature of cryptocurrency transactions.</li>' +
        '<li><b>Protect the security and integrity of our Services</b>. We use your Personal Information, including ' +
        'information about your device and your activity on CoArt to maintain the security of your account and ' +
        'the CoArt platform.</li>' +
        '<li><b>Provide you with customer support</b>. We process your Personal Information when you contact our ' +
        'support team with questions about or issues with your account.</li>' +
        '<li><b>Market our products</b>. We may contact you with information about our Services. We will only do so with ' +
        'your permission, which can be revoked at any time.</li>' +
        '<li><b>Other business purposes</b>. We may use your Personal Information for additional purposes if that purpose ' +
        'is disclosed to you before we collect the information or if we obtain your consent.</li>' +
        '</ul>' +
        '<h2>How We Share Your Personal Information?</h2>' +
        '<p>We will not share your Personal Information with third parties, except as described below:</p>' +
        '<ol>' +
        '<li><b>Service Providers</b>. We may share your Personal Information with third-party service providers for ' +
        'business or commercial purposes, including fraud detection and prevention, security threat detection, ' +
        'payment processing, customer support, data analytics, Information Technology, advertising and marketing, ' +
        'network infrastructure, storage, transaction monitoring. We share your Personal Information with these ' +
        'service providers only so that they can provide us with the services, and we prohibit our service providers ' +
        'from using or disclosing your Personal Information for any other purpose.' +
        '<ol>' +
        '<li>You authorize us to collect and share with our payments provider the Coingate Payment system ' +
        '(Decentralized, UAB). The directory of Lithuanian companies. JSC) your Personal Information including full ' +
        'name, email address and financial information, and you are responsible for the accuracy and completeness ' +
        'of that data. The Coingate Payment system Privacy Policy is available ' +
        '<a href="https://coingate.com/privacy" target="_blank">here</a>.</li>' +
        '</ol>' +
        '</li>' +
        '<li><b>Law Enforcement</b>. We may be compelled to share your Personal Information with law enforcement, ' +
        'government officials, and regulators.</li>' +
        '<li><b>Corporate Transactions</b>. We may disclose Personal Information in the event of a proposed or ' +
        'consummated merger, acquisition, reorganization, asset sale, or similar corporate transaction, or in the ' +
        'event of a bankruptcy or dissolution.</li>' +
        '<li><b>Professional Advisors</b>. We may share your Personal Information with our professional advisors, ' +
        'including legal, accounting, or other consulting services for purposes of audits or to comply with our ' +
        'legal obligations.</li>' +
        '<li><b>Consent</b>. We may share your Personal Information with your consent.</li>' +
        '</ol>' +
        '<p>If we decide to modify the purpose for which your Personal Information is collected and used, ' +
        'we will amend this Privacy Policy.</p>' +
        '<h2>How do we store your data?</h2>' +
        '<p>CoArt securely stores your data at [enter the location and describe security precautions taken].</p>' +
        '<p>CoArt will keep your data for [enter time period]. Once this time period has expired, we will delete ' +
        'your data by [enter how you delete users\' data].</p>' +
        '<h2>Marketing</h2>' +
        '<p>CoArt would like to send you information about products and services of ours that we think you might ' +
        'like, as well as those of our partner companies.</p>' +
        '<ul>' +
        '<li>[List partner companies here]</li>' +
        '</ul>' +
        '<p>If you have agreed to receive marketing, you may always opt out at a later date.</p>' +
        '<p>You have the right at any time to stop CoArt from contacting you for marketing purposes or giving your ' +
        'data to other members of the CoArt Group.</p>' +
        '<p>If you no longer wish to be contacted for marketing purposes, please click here.</p>' +
        '<h2>What are your data protection rights?</h2>' +
        '<p>CoArt would like to make sure you are fully aware of all of your data protection rights. Every user is ' +
        'entitled to the following:</p>' +
        '<ul>' +
        '<li><b>The right to access</b> - You have the right to request CoArt for copies of your personal data. We may ' +
        'charge you a small fee for this service.</li>' +
        '<li><b>The right to rectification</b> - You have the right to request that CoArt correct any information you ' +
        'believe is inaccurate. You also have the right to request CoArt to complete information you believe ' +
        'is incomplete.</li>' +
        '<li><b>The right to erasure</b> - You have the right to request that CoArt erase your personal data, under certain conditions.</li>' +
        '<li><b>The right to restrict processing</b> - You have the right to request that CoArt restrict the processing of your personal data, under certain conditions.</li>' +
        '<li><b>The right to object to processing</b> - You have the right to object to CoArt\'s processing of your personal data, under certain conditions.</li>' +
        '<li><b>The right to data portability</b> - You have the right to request that CoArt transfer the data that we have collected to another organization, or directly to you, under certain conditions.</li>' +
        '</ul>' +
        '<p>If you make a request, we have one month to respond to you. If you would like to exercise any of these ' +
        'rights, please contact us at our email:</p>' +
        '<p>Call us at <a href="tel:+37067985635">+370 67985635</a></p>' +
        '<p>Or write to us: <a href="mailto:coart@coart.biz">coart@coart.biz</a></p>' +
        '<h2>What are cookies?</h2>' +
        '<p>Cookies are text files placed on your computer to collect standard Internet log information and visitor ' +
        'behaviour information. When you visit our websites, we may collect information from you automatically ' +
        'through cookies or similar technology.</p>' +
        '<p>For further information, visit <a href="allaboutcookies.org" target="_blank">allaboutcookies.org</a>.</p>' +
        '<h2>What are cookies?</h2>' +
        '<p>CoArt uses cookies in a range of ways to improve your experience on our website, including:</p>' +
        '<ul>' +
        '<li>Keeping you signed in</li>' +
        '<li>Understanding how you use our website</li>' +
        '</ul>' +
        '<h2>What types of cookies do we use?</h2>' +
        '<p>There are a number of different types of cookies, however, our website uses:</p>' +
        '<ul>' +
        '<li>Functionality - CoArt uses these cookies so that we recognize you on our website and remember your ' +
        'previously selected preferences. These could include what language you prefer and the location you are ' +
        'in. A mix of first-party and third-party cookies are used.</li>' +
        '<li>Advertising - CoArt uses these cookies to collect information about your visit to our website, the ' +
        'content you viewed, the links you followed and information about your browser, device, and your IP address. ' +
        'CoArt sometimes shares some limited aspects of this data with third parties for advertising purposes. ' +
        'We may also share online data collected through cookies with our advertising partners. This means that ' +
        'when you visit another website, you may be shown advertising based on your browsing patterns on our ' +
        'website.</li>' +
        '</ul>' +
        '<h2>How to manage cookies</h2>' +
        '<p>You can set your browser not to accept cookies, and the above website tells you how to remove cookies ' +
        'from your browser. However, in a few cases, some of our website features may not function as a result.</p>' +
        '<h2>Privacy policies of other websites</h2>' +
        '<p>The CoArt website contains links to other websites. Our privacy policy applies only to our website, so ' +
        'if you click on a link to another website, you should read their privacy policy.</p>' +
        '<h2>Changes to our privacy policy</h2>' +
        '<p>CoArt keeps its privacy policy under regular review and places any updates on this web page. This ' +
        'privacy policy was last updated on 9 January 2019.</p>' +
        '<h2>How to contact us</h2>' +
        '<p>If you have any questions about CoArt\'s privacy policy, the data we hold on you, or you would like to ' +
        'exercise one of your data protection rights, please do not hesitate to contact us.</p>' +
        '<p>Email us at: <a href="mailto:"></a></p>' +
        '<p>Call us: <a href="tel:"></a></p>' +
        '<p>Or write to us at:</p>' +
        '<h2>How to contact the appropriate authority</h2>' +
        '<p>Should you wish to report a complaint or if you feel that CoArt has not addressed your concern in a ' +
        'satisfactory manner, you may contact the Information Commissioner\'s Office.</p>' +
        '<p>Email: <a href="mailto:"></a></p>' +
        '<p>Address</p>',
};
