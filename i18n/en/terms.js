module.exports = {
    title: 'Terms of Services',
    text: '<p>Welcome to CoArt Marketplace, owned and operated by CoArt, UAB ("<b>CoArt Labs</b>", "<b>WE</b>", ' +
        '"<b>US</b>" OR "<b>OUR</b>") WEBSITE (<a href="https://coart.market" target="_blank">https://coart.market</a>), ' +
        '("<b>SITE</b>").' +
        '<br>' +
        'THIS AGREEMENT GOVERNS YOUR ACCESS AND USE OF THE SITE.</p>' +
        '<p>This site allows you to sell and purchase Crypto Assets (defined in Section 1. Once you submit an order to ' +
        'purchase a Unique Digital Asset from another user, your order is passed on to the Coingate Payment system ' +
        '(Decentralized, UAB) and the Coingate completes the transaction on your behalf.</p>' +
        '<p>This site also allows you to exhibit your art objects for display or sale, purchase art objects, ' +
        'resell previously acquired art objects, represent the interests of third parties on the platform as an ' +
        'art dealer, or participate as an art expert.</p>' +
        '<p>ALL TRANSACTIONS INITIATED THROUGH OUR SITE ARE FACILITATED AND RUN BY THE COINGATE PAYMENT SYSTEM ' +
        '(DECENTRALIZED, UAB) AND BY USING OUR SERVICES YOU AGREE THAT YOU ARE GOVERNED BY THE TERMS OF SERVICE ' +
        'AND PRIVACY POLICY OF THE COINGATE PAYMENT SYSTEM.</p>' +
        '<p>COART IS A PLATFORM. WE ARE NOT A BROKER, FINANCIAL INSTITUTION, OR CREDITOR. COART FACILITATES ' +
        'TRANSACTIONS BETWEEN THE SELLER AND THE BUYER, AND IT IS ACTING SOLELY AS AN INTERMEDIARY BETWEEN THE ' +
        'USERS (THE SELLER AND THE BUYER).</p>' +
        '<p>COART SERVICE FEE IS APPLICABLE TO SELLERS ONLY AND IT IS SET UP TO 10% OF THE CRYPTO ASSET PRICE.</p>' +
        '<p>Since we have a growing number of services, we sometimes need to describe additional terms for specific ' +
        'services. Those additional terms and conditions, which are available with the relevant services, then ' +
        'become part of your agreement with us if you use those services.</p>' +
        '<p>THIS TERMS OF SERVICE AGREEMENT ("<b>AGREEMENT</b>") IS IMPORTANT AND AFFECTS YOUR LEGAL RIGHTS, SO PLEASE ' +
        'READ IT CAREFULLY.</p>' +
        '<p>BY CLICKING ON THE "I ACCEPT" BUTTON, COMPLETING THE ACCOUNT REGISTRATION PROCESS, USING OUR SERVICES ' +
        'AND/OR PURCHASING OR SELLING CRYPTO ASSETS, YOU AGREE TO BE BOUND BY THIS AGREEMENT AND ALL OF THE TERMS ' +
        'INCORPORATED HEREIN BY REFERENCE. If you do not agree to this Agreement, you may not access or use the ' +
        'Site, purchase or sell the Crypto Assets.</p>' +
        '<p>CoArt reserves the right to change or modify this Agreement at any time and in our sole discretion. ' +
        'If we make changes to this Agreement, we will provide notice of such changes, such as by sending an ' +
        'email notification, providing notice through the Site or updating the "Last Updated" date at the beginning ' +
        'of this Agreement. By continuing to access or use the Site, you confirm your acceptance of the revised ' +
        'Agreement and all of the terms incorporated therein by reference. We encourage you to review the Agreement ' +
        'frequently to ensure that you understand the terms and conditions that apply when you access or use the ' +
        'Site. If you do not agree to the revised Agreement, you may not access or use the Site.</p>' +
        '<h2>1. Definitions</h2>' +
        '<p>"<b>Crypto Assets</b>" refers to unique physical or digital art objects represented on the Symbol blockchain ' +
        'via unique NFT certificates of art objects "<b>You</b>" refers to you, the user, who uses or visits the Site and/or ' +
        'sells or purchases Crypto Assets on the Site. You must be a person (individual) at least 18 years of age to ' +
        'use the Site. If you are a natural person under the age of 18 or you do not agree to these Terms of Service, ' +
        'then you may not use or access the Site. By agreeing to these Terms of Service, you represent and warrant ' +
        'to us: (i) that you are at least 18 years of age; (ii) that you have not previously been suspended or ' +
        'removed from the Site; and (iii) that your registration and your use of the Site is in compliance with ' +
        'any and all applicable laws and regulations. You agree that you will not knowingly allow any individual ' +
        'under the age of 18 or other minimum legal age where it differs under local legal requirements to use ' +
        'or access the Site.</p>' +
        '<h2>2. Privacy Policy</h2>' +
        '<p>Please refer to our <a href="#" target="_blank">Privacy Policy</a> for information about how we ' +
        'collect, use and share information about you.</p>' +
        '<h2>3. Account Registration and Communication Preferences</h2>' +
        '<ol>' +
        '<li>' +
        'If you wish to use the Site - exhibit your art objects for display or sale, purchase art objects, resell ' +
        'previously acquired art objects, represent the interests of third parties on the platform as an art dealer, ' +
        'or participate as an art expert, you will need to register for an account on the Site ("Account"). By ' +
        'creating an Account, you agree to (a) provide accurate, current and complete Account information about ' +
        'yourself, (b) maintain and promptly update from time to time as necessary your Account information, (c) ' +
        'maintain the security of your password and accept all risks of unauthorized access to your Account and the ' +
        'information you provide to us, and (d) immediately notify us if you discover or otherwise suspect any ' +
        'security breaches related to the Site, or your Account. CoArt will block multiple accounts of the same ' +
        'user. Also, you agree that you will not:' +
        '<ol>' +
        '<li>create another account if we’ve disabled one you had unless you have our written permission first;</li>' +
        '<li>buy, sell, rent or lease access to your Account or username unless you have our written permission first;</li>' +
        '<li>share your Account password with anyone;</li>' +
        '<li>log in or try to log in to access the Site through unauthorized third party applications or clients.</li>' +
        '</ol>' +
        '</li>' +
        '<li>CoArt may require you to provide additional information and documents at the request of any competent ' +
        'authority or in case of application of any applicable law or regulation, including laws related to ' +
        'anti-laundering (legalization) of incomes obtained by criminal means, or for counteracting financing of ' +
        'terrorism. CoArt may also require you to provide additional information and documents in cases where it ' +
        'has reasons to believe that:' +
        '<ol>' +
        '<li>Your Account is being used for money laundering or for any other illegal activity;</li>' +
        '<li>You have concealed or reported false identification information and other details; or</li>' +
        '<li>Transactions effected via your Account were affected in breach of this Agreement.</li>' +
        '</ol>' +
        '</li>' +
        '<li>In such cases, CoArt in its sole discretion may pause or cancel your transactions until such ' +
        'additional information and documents are reviewed by CoArt and accepted as satisfying the requirements of ' +
        'applicable law. If you do not provide complete and accurate information and documents in response to such a ' +
        'request, CoArt may refuse to provide the Content (defined in 4.1 below) to you. By creating an Account, you ' +
        'also consent to receive electronic communications from CoArt (e.g., via email or by posting notices to the ' +
        'Site). These communications may include notices about your Account (e.g., password changes and other ' +
        'transactional information) and are part of your relationship with us. You agree that any notices, ' +
        'agreements, disclosures or other communications that we send to you electronically will satisfy any legal ' +
        'communication requirements, including, but not limited to, that such communications be in writing. You ' +
        'should maintain copies of electronic communications from us by printing a paper copy or saving an electronic ' +
        'copy. We may also send you promotional communications via email, including, but not limited to, newsletters, ' +
        'special offers, surveys and other news and information we think will be of interest to you. You may opt out ' +
        'of receiving these promotional emails at any time by following the unsubscribe instructions provided ' +
        'therein.</li>' +
        '<li>You must provide all equipment and software necessary to connect to the Site and services, including ' +
        'but not limited to, a mobile device that is suitable to connect with and use Site and services, in ' +
        'cases where the Site offers a mobile component. You are solely responsible for any fees, including ' +
        'Internet connection or mobile fees, that you incur when accessing the Site or services.</li>' +
        '<li>Notwithstanding anything to the contrary herein, you acknowledge and agree that you shall have no ' +
        'ownership or other property interest in your Account, and you further acknowledge and agree that all ' +
        'rights in and to your Account are and shall forever be owned by and inure to the benefit of CoArt.</li>' +
        '<li>As stated above, your participation in the Site is also subject to the rules available on this page, ' +
        '<a href="#" target="_blank">Site Rules</a>.</li>' +
        '<li>We welcome and encourage you to provide feedback, comments and suggestions for improvements to the ' +
        'Site ("<b>Feedback</b>"). You may submit Feedback by emailing us at ' +
        '<a href="mailto:coart@coart.biz">coart@coart.biz</a> or by other means of ' +
        'communication. Any Feedback you submit to us will be considered non-confidential and non-proprietary to ' +
        'you. By submitting Feedback to us, you grant us a non-exclusive, worldwide, royalty-free, irrevocable, ' +
        'sub-licensable, perpetual license to use and publish those ideas and materials for any purpose, ' +
        'without compensation to you.</li>' +
        '</ol>' +
        '<h2>4. Ownership</h2>' +
        '<ol>' +
        '<li>Unless otherwise indicated in writing by us, content and other materials contained therein, including, ' +
        'without limitation, the CoArt logo and all designs, text, graphics, pictures, information, data, software, ' +
        'sound files, other files and the selection and arrangement thereof (collectively, "Content"), the Site, and ' +
        'any Crypto Assets are the proprietary property of CoArt or our affiliates, licensors or users, as applicable.</li>' +
        '<li>Notwithstanding anything to the contrary in this Agreement, the Site and Content may include software ' +
        'components provided by CoArt or its affiliates or a third party that are subject to separate license terms, ' +
        'in which case those license terms will govern such software components.</li>' +
        '<li>The CoArt logo and any CoArt product or service names, logos or slogans that may appear on the Site or ' +
        'Service are trademarks of CoArt or our affiliates and may not be copied, imitated or used, in whole or in ' +
        'part, without our prior written permission. You will not use, copy, adapt, modify, prepare derivative works ' +
        'of, distribute, license, sell, transfer, publicly display, publicly perform, transmit, broadcast or ' +
        'otherwise exploit the Site or Content. You may not use any metatags or other "hidden text" utilizing "CoArt" ' +
        'or any other name, trademark or product or service name of CoArt or our affiliates without our prior written ' +
        'permission. In addition, the look and feel of the Site and Content, including, without limitation, all page ' +
        'headers, custom graphics, button icons and scripts, constitute the service mark, trademark or trade dress ' +
        'of CoArt and may not be copied, imitated or used, in whole or in part, without our prior written ' +
        'permission. All other trademarks, registered trademarks, product names and CoArt names or logos ' +
        'mentioned on the Site are the property of their respective owners and may not be copied, imitated or ' +
        'used, in whole or in part, without the permission of the applicable trademark holder. Reference to any ' +
        'products, services, processes or other information by name, trademark, manufacturer, supplier or otherwise ' +
        'does not constitute or imply endorsement, sponsorship or recommendation by CoArt.</li>' +
        '<li>No licenses or rights are granted to you by implication or otherwise under any intellectual property ' +
        'rights owned or controlled by CoArt or its licensors, except for the licenses and rights expressly granted ' +
        'in these Terms.</li>' +
        '</ol>' +
        '<h2>5. License to Access and Use Our Site and Content; License to Crypto Assets</h2>' +
        '<ol>' +
        '<li>You are hereby granted a limited, nonexclusive, nontransferable, revocable, non-sublicensable license ' +
        'to access and use the Site and Content. However, such license is subject to this Agreement and does not ' +
        'include any right to (a) sell, resell or use commercially the Site or Content, (b) distribute, publicly ' +
        'perform or publicly display any Content, (c) modify or otherwise make any derivative uses of the Site or ' +
        'Content, or any portion thereof, (d) use any data mining, robots or similar data gathering or extraction ' +
        'methods, (e) download (other than page caching) any portion of the Site or Content, except as expressly ' +
        'permitted by us, and (f) use the Site or Content other than for their intended purposes.</li>' +
        '<li>CoArt does not claim ownership of your User Materials or your Crypto Assets. When you upload content to ' +
        'the Site, including any Crypto Assets, you are and remain the owner of your User Materials and your Crypto ' +
        'Assets. However, when you as a user create, upload, send, receive, post, publish or store your User ' +
        'Materials, such as text, photos, audio, visual works, video or other materials and information ' +
        '("<b>User Materials</b>"), on, through or in the Site, you represent that (a) you either are the sole ' +
        'and exclusive owner of all User Materials that you make available on or through the Site; (b) you have ' +
        'all rights, licenses, consents and releases that are necessary to grant to CoArt the rights in and to such ' +
        'User Materials, as contemplated under these Terms, including without limitation, that you have a ' +
        'royalty-free, perpetual, irrevocable, worldwide, non-exclusive right (including any moral rights) and ' +
        'license to use, license, reproduce, modify, adapt, publish, translate, create derivative works from, ' +
        'distribute, derive revenue or other remuneration from, and communicate to the public, perform and display ' +
        'your User Materials (in whole or in part) worldwide and/or to incorporate it in other works in any form, ' +
        'media or technology now known or later developed, for the full term of any worldwide intellectual property ' +
        'right that may exist in your User Materials; (c) neither the User Materials nor your posting, uploading, ' +
        'publication, submission or transmittal of the User Materials or CoArt’s use of the User Materials (or any ' +
        'portion thereof) will infringe, misappropriate or violate a third party\'s patent, copyright, trademark, ' +
        'trade secret, moral rights or other proprietary or intellectual property rights, or rights of publicity ' +
        'or privacy, or result in the violation of any applicable law or regulation.</li>' +
        '<li>By creating, uploading, posting, sending, receiving, storing, or otherwise making available any User ' +
        'Materials and your Crypto Assets on, in or through the Site, you grant to CoArt a non-exclusive, worldwide, ' +
        'royalty-free, license to such User Materials to access, use, store, copy, modify, prepare derivative works ' +
        'of, distribute, publish, transmit, stream, broadcast, and otherwise distribute such User Materials solely ' +
        'for the purpose of providing and/or promoting the Site and, featuring your User Materials and your Crypto ' +
        'Assets within our Site and promoting it through our marketing ecosystem. To the extent applicable and ' +
        'permissible by law, you hereby waive any and all claims that you may now or hereafter have in any ' +
        'jurisdiction to so-called “moral rights” or right of “Droit moral” with respect to any of your User ' +
        'Materials. You may request to remove your User Materials in accordance with our ' +
        '<a href="#" target="_blank">Privacy Policy</a>, which is hereby incorporated by reference. The rights ' +
        'granted in this Section 5c will survive the termination or expiration of this Agreement.</li>' +
        '<li>This Crypto Asset is a limited-edition digital creation. Unless otherwise specified by the Seller of a ' +
        'Crypto Asset in writing, your purchase of a Crypto Asset does not give you the right to publicly display, ' +
        'perform, distribute, sell or otherwise reproduce the Crypto Asset for any commercial purpose. You further ' +
        'agree that you are not receiving any copyright interest in the Crypto Asset. Any commercial exploitation ' +
        'of the Crypto Assets could subject You to claims of copyright infringement. If you sell a Crypto Asset ' +
        'through the Site, you agree that you will not have any claims against CoArt for any breach of Section 5d ' +
        'by a purchaser. If you purchase a Crypto Asset on the Site, You hereby agree to hold CoArt and the seller ' +
        'of such Crypto Assets harmless from and against any and all violations or breaches of this Section 5d.</li>' +
        '<li>We have the right to remove or refuse to post any User Materials, including Crypto Assets, (a) for ' +
        'any or no reason in our sole discretion; (b) take any action with respect to any User Materials that we ' +
        'deem necessary or appropriate in our sole discretion, including if we believe that such User Materials ' +
        'violates the Agreement, infringes any intellectual property right of any person or entity, threatens the ' +
        'personal safety of users of the Site or the public, or could create liability for CoArt; (c) disclose your ' +
        'identity or other information about you to any third party who claims that material posted by you ' +
        'violates their rights, including their intellectual property rights or their right to privacy; (d) take ' +
        'appropriate legal action, including without limitation, referral to law enforcement, for any illegal or ' +
        'unauthorized use of the Site; and (e) terminate or suspend your access to all or part of the Site for ' +
        'any or no reason, including without limitation, any violation of this Agreement. Without limiting the ' +
        'foregoing, we have the right to cooperate fully with any law enforcement authorities or court order ' +
        'requesting or directing us to disclose the identity or other information of anyone posting any materials ' +
        'on or through the Site. YOU WAIVE AND HOLD HARMLESS COART AND ITS AFFILIATES, LICENSEES, AND SERVICE ' +
        'PROVIDERS, FROM ANY CLAIMS RESULTING FROM ANY ACTION TAKEN BY ANY OF THE FOREGOING PARTIES DURING ' +
        'OR TAKEN AS A CONSEQUENCE OF, INVESTIGATIONS BY EITHER SUCH PARTIES OR LAW ENFORCEMENT AUTHORITIES.</li>' +
        '<li>You understand and agree that we have the right to terminate or suspend your access to all or part of ' +
        'the Site for any or no reason, including without limitation, any violation of this Agreement.</li>' +
        '<li>CoArt shall have the right, but not the obligation, to monitor the content of the offerings, to ' +
        'determine compliance with these Terms and any operating rules established by CoArt and to satisfy any ' +
        'law, regulation or authorized government request. CoArt shall have the right in its sole discretion to ' +
        'edit, refuse to post or remove any material submitted to or posted through the Offerings. Without limiting ' +
        'the foregoing, CoArt shall have the right to remove any material that CoArt, in its sole discretion, finds ' +
        'to be in violation of the provisions hereof or otherwise objectionable.</li>' +
        '<li>However, we cannot undertake to review all User Materials before it is posted on the Site, and cannot ' +
        'ensure prompt removal of objectionable User Material after it has been posted. Accordingly, we assume no ' +
        'liability for any action regarding transmissions, communications, or content provided by any user or ' +
        'third party.</li>' +
        '</ol>' +
        '<h2>6. Third-Party Content Policy & Complaint Procedures</h2>' +
        '<ol>' +
        '<li>Third-Party Content Policy. As a matter of policy, we do not tolerate any User Materials posted to the ' +
        'Site that, in our sole discretion: infringes intellectual property rights; violates the law; constitutes ' +
        'child pornography; or is obscene or defamatory. We intend to, in good faith, remove, disable or restrict ' +
        'access to, or the availability of, User Materials that, in our sole discretion, we deem infringing, ' +
        'racist, obscene, obscene as to minors, child pornography, lewd, lascivious, filthy, excessively violent, ' +
        'harassing, or otherwise objectionable. The provisions of this section are intended to implement this policy ' +
        'but are not intended to impose a contractual obligation on us to undertake or refrain from any particular ' +
        'course of conduct.</li>' +
        '<li>' +
        '<p>Third-Party Content Complaints. If you believe that someone has posted User Materials that violates this ' +
        'policy (other than in cases of copyright infringement, which is addressed separately below), we ask you to ' +
        'promptly notify us by email at the following address: coart@coart.biz. You must use this address if you ' +
        'want to ensure that your complaint is actually received by the appropriate person charged with ' +
        'investigating alleged policy violations.</p>' +
        '<p>In order to allow us to respond effectively, please provide us with as much detail as possible, ' +
        'including: (1) the nature of the right infringed or violated (including the registration numbers of any ' +
        'registered trademarks or patents allegedly infringed); (2) all facts which lead you to believe that a right ' +
        'has been violated or infringed; (3) the precise location where the offending User Materials can be found; ' +
        '(4) any grounds to believe that the person who posted the User Materials was not authorized to do so or ' +
        'did not have a valid defence (including the defence of fair use); and (5) if known, the identity of the ' +
        'person or persons who posted the infringing or offending User Materials.</p>' +
        '<p>By lodging a complaint, you agree that the substance of your complaint shall be deemed to constitute a ' +
        'representation made under penalty of perjury under the laws of the State of Delaware. In addition, ' +
        'you agree, at your own expense, to defend and indemnify us and hold us harmless against all claims which ' +
        'may be asserted against us, and all losses incurred, as a result of your complaint and/or our response to it.</p>' +
        '<p>We expect visitors to take responsibility for their own actions and cannot assume liability for any acts ' +
        'of third parties which take place on the CoArt site. By taking advantage of the Good Samaritan procedures ' +
        'set forth in this Section, you waive any and all claims or remedies that you might otherwise be able to ' +
        'assert against us under any theory of law (including, without limitation, intellectual property laws) that ' +
        'arise out of or relate in any way to the User Materials at the site or our response, or failure to respond, ' +
        'to a complaint.</p>' +
        '<p>You agree that we have the right (but not the obligation) to investigate any complaint received. By ' +
        'reserving this right, we do not undertake any responsibility in fact to investigate complaints or to ' +
        'remove, disable or restrict access to or the availability of User Materials. We support free speech on the ' +
        'Internet and therefore will not act on complaints that we believe, in our subjective judgment, to be ' +
        'deficient. If you believe that User Materials remain on the site that violates your rights, your sole ' +
        'remedy shall be against the person(s) responsible for posting or storing it, not against us.</p>' +
        '</li>' +
        '</ol>' +
        '<h2>7. Hyperlinks</h2>' +
        '<p>You are granted a limited, nonexclusive, revocable, non-transferable, non-sublicensable right to create ' +
        'a text hyperlink to the Site for noncommercial purposes, provided that such link does not portray CoArt or ' +
        'our affiliates or any of our products or services in a false, misleading, derogatory or otherwise defamatory ' +
        'manner, and provided further that the linking site does not contain any adult or illegal material or any ' +
        'material that is offensive, harassing or otherwise objectionable. This limited right may be revoked at any ' +
        'time. You may not use a logo or other proprietary graphic of CoArt to link to the Site or Content without ' +
        'our express written permission. Further, you may not use, frame or utilize framing techniques to enclose ' +
        'any CoArt trademark, logo or other proprietary information, including the images found on the Site, the ' +
        'content of any text or the layout or design of any page, or form contained on a page, on the Site without ' +
        'our express written consent.</p>' +
        '<h2>8. Third-Party Services</h2>' +
        '<p>The Site may contain links to third-party websites ("<b>Third-Party Websites</b>") and applications ' +
        '("<b>Third-Party Applications</b>"). When you click on a link to a Third-Party Website or Third-Party Application, ' +
        'we will not warn you that you have left our Site and are subject to the Agreement and conditions ' +
        '(including privacy policies) of another website or destination. Such Third-Party Websites and Third-Party ' +
        'Applications and are not under the control of CoArt. CoArt is not responsible for any Third-Party Websites ' +
        'or Third-Party Applications. CoArt provides these Third-Party Websites and Third-Party Applications only as ' +
        'a convenience and does not review, approve, monitor, endorse, warrant, or make any representations with ' +
        'respect to Third-Party Websites or Third-Party Applications, or their products or services. You use all ' +
        'links in Third-Party Websites and Third-Party Applications at your own risk. When you leave our Site, ' +
        'our Agreement and policies no longer govern. You should review applicable agreements and policies, ' +
        'including privacy and data gathering practices, of any Third-Party Websites or Third-Party Applications, ' +
        'and should make whatever investigation you feel necessary or appropriate before proceeding with any ' +
        'transaction with any third party.</p>' +
        '<h2>9. User Conduct</h2>' +
        '<p>You agree that you will not violate any law, contract, intellectual property or other third party ' +
        'rights and that you are solely responsible for your conduct while accessing or using the Site. You agree ' +
        'that you will abide by this Agreement and will not:</p>' +
        '<ol>' +
        '<li>Use the Crypto Assets in any way that is contrary to your grant of rights in the Crypto Asset;</li>' +
        '<li>Provide false or misleading information to CoArt;</li>' +
        '<li>Use or attempt to use another user’s Account without authorization from such user and CoArt;</li>' +
        '<li>Use the Site in any manner that could interfere with, disrupt, negatively affect or inhibit other ' +
        'users from fully enjoying the Site, or that could damage, disable, overburden or impair the functioning ' +
        'of the Site in any manner;</li>' +
        '<li>Reverse engineer any aspect of the Site, or do anything that might discover source code or bypass or ' +
        'circumvent measures employed to prevent or limit access to any Service, area or code of the Site;</li>' +
        '<li>Attempt to circumvent any content-filtering techniques we employ or attempt to access any feature or ' +
        'area of the Site that you are not authorized to access;</li>' +
        '<li>Use any robot, spider, crawler, scraper, script, browser extension, offline reader or other automated ' +
        'means or interface not authorized by us to access the Site, extract data or otherwise interfere with or ' +
        'modify the rendering of Site pages or functionality;</li>' +
        '<li>Use data collected from our Site to contact individuals, companies, or other persons or entities;</li>' +
        '<li>Use data collected from our Site for any direct marketing activity (including without limitation, ' +
        'email marketing, SMS marketing, telemarketing, and direct marketing);</li>' +
        '<li>Bypass or ignore instructions that control all automated access to the Site; or</li>' +
        '<li>Use the Site for any illegal or unauthorized purpose, or engage in, encourage or promote any activity ' +
        'that violates this Agreement.</li>' +
        '<li>Manipulate the price of any item (e.g., Crypto Asset) or interfere with any other user\'s listing or ' +
        'profile;</li>' +
        '<li>Engage in any coercive, deceptive, and/or manipulative behaviour, including using coercive, deceptive, ' +
        'and/or manipulative tactics;</li>' +
        '<li>Post false, inaccurate, misleading, deceptive, defamatory, or libellous content;</li>' +
        '<li>Distribute or post spam, unsolicited or bulk electronic communications, chain letters, or pyramid ' +
        'schemes;</li>' +
        '<li>Distribute viruses or any other technologies that may harm CoArt or the interests or property of users;</li>' +
        '<li>Post any hateful content;</li>' +
        '<li>Use the Site to carry out any illegal activities, including but not limited to money laundering, ' +
        'terrorist financing or deliberately engaging in activities designed to adversely affect the performance ' +
        'of the Site.</li>' +
        '</ol>' +
        '<h2>10. Indemnification</h2>' +
        '<p>To the fullest extent permitted by applicable law, you agree to indemnify, defend and hold harmless ' +
        'CoArt, and our respective past, present and future employees, officers, directors, contractors, consultants, ' +
        'equity holders, suppliers, vendors, service providers, parent companies, subsidiaries, affiliates, agents, ' +
        'representatives, predecessors, successors and assigns (individually and collectively, the "<b>CoArt Parties</b>"), ' +
        'from and against all actual or alleged third party claims, damages, awards, judgments, losses, liabilities, ' +
        'obligations, penalties, interest, fees, expenses (including, without limitation, attorneys’ fees and ' +
        'expenses) and costs (including, without limitation, court costs, costs of settlement and costs of pursuing ' +
        'indemnification and insurance), of every kind and nature whatsoever, whether known or unknown, foreseen or ' +
        'unforeseen, matured or unmatured, or suspected or unsuspected, in law or equity, whether in tort, contract ' +
        'or otherwise (collectively, "<b>Claims</b>"), including, but not limited to, damages to property or personal ' +
        'injury, that are caused by, arise out of or are related to (a) your use or misuse of the Site, Content or ' +
        'Crypto Assets, (b) any Feedback you provide, (c) your violation of this Agreement, and (d) your violation ' +
        'of the rights of a third party, including another user or MetaMask. You agree to promptly notify CoArt ' +
        'of any third party Claims and cooperate with the CoArt Parties in defending such Claims. You further agree ' +
        'that the CoArt Parties shall have control of the defence or settlement of any third party Claims. THIS ' +
        'INDEMNITY IS IN ADDITION TO, AND NOT IN LIEU OF, ANY OTHER INDEMNITIES SET FORTH IN A WRITTEN AGREEMENT ' +
        'BETWEEN YOU AND COART.</p>' +
        '<h2>11. Disclaimers</h2>' +
        '<p>EXCEPT AS EXPRESSLY PROVIDED TO THE CONTRARY IN A WRITING BY COART, THE SITE, CONTENT CONTAINED THEREIN, ' +
        'AND CRYPTO ASSETS LISTED THEREIN ARE PROVIDED ON AN "AS IS" AND "AS AVAILABLE" BASIS WITHOUT WARRANTIES OR ' +
        'CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED. COART (AND ITS SUPPLIERS) MAKE NO WARRANTY THAT THE ' +
        'SITE: A) WILL MEET YOUR REQUIREMENTS; (B) WILL BE AVAILABLE ON AN UNINTERRUPTED, TIMELY, SECURE, OR ' +
        'ERROR-FREE BASIS; OR (C) WILL BE ACCURATE, RELIABLE, COMPLETE, LEGAL, OR SAFE. COART DISCLAIMS ALL OTHER ' +
        'WARRANTIES OR CONDITIONS, EXPRESS OR IMPLIED, INCLUDING, WITHOUT LIMITATION, IMPLIED WARRANTIES OR ' +
        'CONDITIONS OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT AS TO THE SITE, ' +
        'CONTENT CONTAINED THEREIN. COART DOES NOT REPRESENT OR WARRANT THAT CONTENT ON THE SITE IS ACCURATE, ' +
        'COMPLETE, RELIABLE, CURRENT OR ERROR-FREE. WE WILL NOT BE LIABLE FOR ANY LOSS OF ANY KIND FROM ANY ACTION ' +
        'TAKEN OR TAKEN IN RELIANCE ON MATERIAL OR INFORMATION, CONTAINED ON THE SITE. WHILE COART ATTEMPTS TO MAKE ' +
        'YOUR ACCESS TO AND USE OF THE SITE AND CONTENT SAFE, COART CANNOT AND DOES NOT REPRESENT OR WARRANT THAT ' +
        'THE SITE, CONTENT, ANY CRYPTO ASSETS LISTED ON OUR SITE OR OUR SERVERS ARE FREE OF VIRUSES OR OTHER HARMFUL ' +
        'COMPONENTS. WE CANNOT GUARANTEE THE SECURITY OF ANY DATA THAT YOU DISCLOSE ONLINE. YOU ACCEPT THE INHERENT ' +
        'SECURITY RISKS OF PROVIDING INFORMATION AND DEALING ONLINE OVER THE INTERNET AND WILL NOT HOLD US ' +
        'RESPONSIBLE FOR ANY BREACH OF SECURITY UNLESS IT IS DUE TO OUR GROSS NEGLIGENCE.</p>' +
        '<p>WE WILL NOT BE RESPONSIBLE OR LIABLE TO YOU FOR ANY LOSS AND TAKE NO RESPONSIBILITY FOR, AND WILL NOT BE ' +
        'LIABLE TO YOU FOR, ANY USE CRYPTO ASSETS OF INCLUDING BUT NOT LIMITED TO ANY LOSSES, DAMAGES OR CLAIMS ' +
        'ARISING FROM: (A) USER ERROR SUCH AS FORGOTTEN PASSWORDS, INCORRECTLY CONSTRUCTED TRANSACTIONS, OR ' +
        'MISTYPED ADDRESSES; (B) SERVER FAILURE OR DATA LOSS; (C) CORRUPTED WALLET FILES; (D) UNAUTHORIZED ACCESS ' +
        'TO APPLICATIONS; (E) ANY UNAUTHORIZED THIRD-PARTY ACTIVITIES, INCLUDING WITHOUT LIMITATION THE USE OF ' +
        'VIRUSES, PHISHING, BRUTE FORCING OR OTHER MEANS OF ATTACK AGAINST THE SITE OR CRYPTO ASSETS.</p>' +
        '<p>CoArt is not responsible for sustained casualties due to vulnerability or any kind of failure, ' +
        'abnormal behaviour of software (e.g., wallet, smart contract), blockchains or any other features of the ' +
        'Crypto Assets. CoArt is not responsible for casualties due to late reports by developers or representatives ' +
        '(or no report at all) of any issues with the blockchain supporting Crypto Assets including forks, technical ' +
        'node issues or any other issues having fund losses as a result.</p>' +
        '<p>Nothing in this Agreement shall exclude or limit the liability of either party for fraud, death or ' +
        'bodily injury caused by negligence, violation of laws, or any other activity that cannot be limited or ' +
        'excluded by legitimate means.</p>' +
        '<h2>11. Assumption of Risk</h2>' +
        '<p>You accept and acknowledge:</p>' +
        '<ol>' +
        '<li>The prices of blockchain assets are extremely volatile. Fluctuations in the price of other digital ' +
        'assets could materially and adversely affect the Crypto Assets, which may also be subject to significant ' +
        'price volatility. We cannot guarantee that any purchasers of Crypto Assets will not lose money.</li>' +
        '<li>You are solely responsible for determining what, if any, taxes apply to your Crypto Assets transactions. ' +
        'Neither CoArt nor any other CoArt Party is responsible for determining the taxes that apply to Crypto Assets ' +
        'transactions.</li>' +
        '<li>Our Site does not store, send, or receive Crypto Assets. This is because Crypto Assets exist only by ' +
        'virtue of the ownership record maintained on its supporting blockchain. Any transfer of Crypto Assets ' +
        'occurs within the supporting blockchain and not on this Site.</li>' +
        '<li>There are risks associated with using an Internet based currency, including but not limited to, the ' +
        'risk of hardware, software and Internet connections, the risk of malicious software introduction, and the ' +
        'risk that third parties may obtain unauthorized access to information stored within your wallet. You accept ' +
        'and acknowledge that CoArt will not be responsible for any communication failures, disruptions, errors, ' +
        'distortions or delays you may experience when using the Crypto Assets, however, caused.</li>' +
        '<li>A lack of use or public interest in the creation and development of distributed ecosystems could ' +
        'negatively impact the development of the Bread Ecosystem and therefore the potential utility or value ' +
        'of Crypto Assets.</li>' +
        '<li>The regulatory regime governing blockchain technologies, cryptocurrencies, and tokens is uncertain, and ' +
        'new regulations or policies may materially adversely affect the development of the Site and the utility of ' +
        'Crypto Assets.</li>' +
        '<li>The Site will rely on third-party platforms such as the Coingate Payment System to perform the ' +
        'transactions of Crypto Assets. If we are unable to maintain a good relationship with such platform ' +
        'providers; if the terms and conditions or pricing of such platform providers change; if we violate or ' +
        'cannot comply with the terms and conditions of such platforms; or if any of such platforms lose market ' +
        'share or falls out of favour or is unavailable for a prolonged period of time, access to and use of the ' +
        'Site will suffer.</li>' +
        '</ol>' +
        '<h2>12. Limitation of Liability; Release</h2>' +
        '<p>TO THE FULLEST EXTENT PERMITTED BY LAW, IN NO EVENT WILL COART BE LIABLE TO YOU OR ANY THIRD PARTY FOR ' +
        'ANY LOST PROFIT OR ANY INDIRECT, CONSEQUENTIAL, EXEMPLARY, INCIDENTAL, SPECIAL OR PUNITIVE DAMAGES ARISING ' +
        'FROM THIS AGREEMENT, THE SITE, PRODUCTS OR THIRD PARTY SITES AND PRODUCTS, OR FOR ANY DAMAGES RELATED TO ' +
        'LOSS OF REVENUE, LOSS OF PROFITS, LOSS OF BUSINESS OR ANTICIPATED SAVINGS, LOSS OF USE, LOSS OF GOODWILL, ' +
        'OR LOSS OF DATA, AND WHETHER CAUSED BY TORT (INCLUDING NEGLIGENCE), BREACH OF CONTRACT OR OTHERWISE, EVEN ' +
        'IF FORESEEABLE AND EVEN IF COART HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. ACCESS TO, AND USE OF, ' +
        'THE SITES, PRODUCTS OR THIRD PARTY SITES AND PRODUCTS ARE AT YOUR OWN DISCRETION AND RISK, AND YOU WILL BE ' +
        'SOLELY RESPONSIBLE FOR ANY DAMAGE TO YOUR COMPUTER SYSTEM OR MOBILE DEVICE OR LOSS OF DATA RESULTING ' +
        'THEREFROM.</p>' +
        '<p>NOTWITHSTANDING ANYTHING TO THE CONTRARY CONTAINED HEREIN, IN NO EVENT SHALL THE MAXIMUM AGGREGATE ' +
        'LIABILITY OF COART ARISING OUT OF OR IN ANY WAY RELATED TO THIS AGREEMENT, THE ACCESS TO AND USE OF THE ' +
        'SITE, CONTENT, CRYPTO ASSETS, OR ANY PRODUCTS OR SERVICES PURCHASED ON THE SITE EXCEED THE GREATER OF \</p>' +
        '<p>(A) THE EQUIVALENT OF $100 OR</p>' +
        '<p>(B) THE AMOUNT RECEIVED BY COART FROM THE SALE OF CRYPTO ASSETS THAT ARE THE SUBJECT OF THE CLAIM.</p>' +
        '<p>THE FOREGOING LIMITATIONS OF LIABILITY SHALL NOT APPLY TO LIABILITY OF COART FOR (A) DEATH OR PERSONAL ' +
        'INJURY CAUSED BY A MEMBER OF COART’S NEGLIGENCE; OR FOR (B) ANY INJURY CAUSED BY A MEMBER OF COART’S FRAUD ' +
        'OR FRAUDULENT MISREPRESENTATION.</p>' +
        '<h2>13. Modifications to the Site</h2>' +
        '<p>We reserve the right in our sole discretion to modify, suspend or discontinue, temporarily or ' +
        'permanently, the Sites (or any features or parts thereof) or suspend or discontinue the operation at ' +
        'any time and without liability therefor.</p>' +
        '<h2>14. Governing Law and Venue</h2>' +
        '<p>This Agreement, your access to and use of the Site and Content, and your participation therein shall ' +
        'be governed by and construed and enforced in accordance with the laws of the Republic of Lithuania, without ' +
        'regard to conflict of law rules or principles of the Republic of Lithuania, or any other jurisdiction) that ' +
        'would cause the application of the laws of any other jurisdiction. Any Dispute between the parties that is ' +
        'not subject to arbitration or cannot be heard in small claims court, shall be resolved in courts of the ' +
        'Republic of Lithuania.</p>' +
        '<h2>15. Termination</h2>' +
        '<p>Notwithstanding anything contained in this Agreement, we reserve the right, without notice and in our ' +
        'sole discretion, to terminate your right to access or use the Site at any time and for any or no reason, ' +
        'and you acknowledge and agree that we shall have no liability or obligation to you in such event and that ' +
        'you will not be entitled to a refund of any amounts that you have already paid to us, to the fullest extent ' +
        'permitted by applicable law.</p>' +
        '<h2>16. Severability</h2>' +
        '<p>If any term, clause or provision of this Agreement is held invalid or unenforceable, then that term, ' +
        'clause or provision will be severable from this Agreement and will not affect the validity or ' +
        'enforceability of any remaining part of that term, clause or provision, or any other term, clause or ' +
        'provision of this Agreement.</p>' +
        '<h2>16. Survival</h2>' +
        '<p>The following sections will survive the expiration or termination of this Agreement and the termination ' +
        'of your Account: all defined terms and Sections 2, 4, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, and 20.</p>' +
        '<h2>17. Miscellaneous</h2>' +
        '<p>This Agreement along with the Site Rules constitutes the entire agreement between you and CoArt relating ' +
        'to your access to and use of the Sites and Content. This Agreement, and any rights and licenses granted ' +
        'hereunder, may not be transferred or assigned by you without the prior written consent of CoArt prior, ' +
        'concurrent or subsequent circumstance, and CoArt’s failure to assert any right or provision under this ' +
        'Agreement shall not constitute a waiver of such right or provision. Except as otherwise provided herein, ' +
        'this Agreement is intended solely for the benefit of the parties and are not intended to confer third party ' +
        'beneficiary rights upon any other person or entity.</p>',
};
