const messages = require('./i18n/messages');

export default {
    target: 'server',
    head: {
        title: 'CoArt Whitelist sale',
        meta: [
            {
                charset: 'utf-8',
            },
            {
                name: 'viewport',
                content: 'width=device-width, initial-scale=1',
            },
            {
                hid: 'description',
                name: 'description',
                content: 'There is the first stage of CoArt Whitelist tokensale. You are welcome!',
            },
            {
                hid: 'twitter:title',
                name: 'twitter:title',
                content: 'CoArt Whitelist sale',
            },
            {
                hid: 'twitter:description',
                name: 'twitter:description',
                content: 'There is the first stage of CoArt Whitelist tokensale. You are welcome!',
            },
            {
                hid: 'twitter:site',
                name: 'twitter:site',
                content: '@coart_market',
            },
            {
                hid: 'og:title',
                name: 'og:title',
                content: 'CoArt Whitelist sale',
            },
            {
                hid: 'og:description',
                name: 'og:description',
                content: 'There is the first stage of CoArt Whitelist tokensale. You are welcome!',
            },
            {
                hid: 'og:locale',
                name: 'og:locale',
                content: 'en_US',
            },
            {
                hid: 'og:type',
                name: 'og:type',
                content: 'website',
            },
        ],
        link: [
            {
                rel: 'icon',
                type: 'image/x-icon',
                href: '/favicon.ico',
            },
        ],
    },
    env: {
        buildDir: '@/.nuxt/',
    },
    loading: {
        color: '#000000',
        height: '2px',
        throttle: 0,
    },
    /*
    ** Global CSS
    */
    css: [
        '~/assets/css/normalize.css',
    ],
    plugins: [
        '~/plugins/directives.js',
        '~/plugins/global.js',
    ],
    components: true,
    /*
    ** Nuxt.js dev-modules
    */
    buildModules: [
        '@nuxt/typescript-build',
        'nuxt-typed-vuex',
        '@nuxtjs/stylelint-module',
        '@nuxtjs/device',
    ],
    device: {
        refreshOnResize: true,
    },
    /*
    ** Nuxt.js modules
    */
    modules: [
        '@nuxtjs/axios',
        '@nuxtjs/toast',
        '@nuxtjs/moment',
        '@nuxtjs/sitemap',
        'nuxt-i18n',
        [
            '@nuxtjs/robots',
            {
                UserAgent: '*',
            },
        ],
        [
            'nuxt-fontawesome',
            {
                component: 'fa',
                imports: [
                    {
                        set: '@fortawesome/free-solid-svg-icons',
                        icons: [
                            'faShoppingCart',
                            'faInfoCircle',
                            'faEuroSign',
                            'faChevronDown',
                            'faPlus',
                            'faTrashAlt',
                        ],
                    },
                    {
                        set: '@fortawesome/free-regular-svg-icons',
                        icons: [
                            'faClone',
                            'faEye',
                            'faHeart',
                            'faUser',
                            'faCreditCard',
                            'faImages',
                            'faQuestionCircle',
                        ],
                    },
                    {
                        set: '@fortawesome/free-brands-svg-icons',
                        icons: [
                            'faFacebook',
                            'faInstagram',
                            'faLinkedin',
                            'faTwitter',
                            'faTelegram',
                            'faMedium',
                            'faReddit',
                            'faYoutube',
                            'faGithub',
                        ],
                    },
                ],
            },
        ],
    ],
    router: {
        linkExactActiveClass: 'link-exact-active',
    },
    i18n: {
        seo: false,
        baseUrl: process.env.BASE_URL,
        locales: [{
            code: 'en',
            iso: 'en-US',
            isCatchallLocale: true,
        }],
        defaultLocale: 'en',
        vueI18n: {
            fallbackLocale: 'en',
            messages,
        },
    },
    toast: {
        position: 'top-right',
        duration: 5000,
    },
    sitemap: {},
    axios: {},
    /*
    ** Build configuration
    */
    build: {
        transpile: [
            /typed-vuex/,
        ],
    },
    server: {
        port: process.env.PORT || 4080,
        host: 'localhost',
    },
};
