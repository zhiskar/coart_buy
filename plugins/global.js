import Vue from 'vue';

import Container from '~/components/grid/Container';
import Row from '~/components/grid/Row';
import Column from '~/components/grid/Column';

Vue.component('container', Container);
Vue.component('row', Row);
Vue.component('column', Column);
