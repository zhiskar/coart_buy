import { actionTree, getAccessorType, mutationTree } from 'nuxt-typed-vuex';

export const state = () => ({
    _token: '' as string,
});

type RootState = ReturnType<typeof state>;

export const getters = {
    token: (state): String => state._token,
};

export const mutations = mutationTree(state, {
    setToken(state, token: string) {
        state._token = token;
    },
});

export const actions = actionTree(
    {
        state,
        getters,
        mutations,
    },
    {},
);

export const accessorType = getAccessorType({
    actions,
    getters,
    mutations,
    state,
    modules: {},
});
