module.exports = {
    extends: [
        'stylelint-config-standard',
        'stylelint-scss',
        'stylelint-config-recommended-scss',
    ],
    defaultSeverity: 'error',
    rules: {
        indentation: 4,
        'at-rule-name-space-after': 'always-single-line',
        'number-leading-zero': 'never',
        'value-list-comma-newline-after': null,
        'at-rule-no-unknown': null,
        'no-empty-source': null,
        'selector-pseudo-element-no-unknown': [
            true,
            {
                ignorePseudoElements: [
                    'v-deep',
                ],
            },
        ],
    },
};
